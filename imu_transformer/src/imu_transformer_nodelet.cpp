#include "imu_transformer/imu_transformer_nodelet.h"
#include "pluginlib/class_list_macros.h"
#include "geometry_msgs/Vector3Stamped.h"

//#include "tf2_sensor_msgs/tf2_sensor_msgs.h"
//Remove this header when https://github.com/ros/geometry_experimental/pull/78 is released
#include "imu_transformer/tf2_sensor_msgs.h"
#include "math.h"


namespace imu_transformer
{

  // Eigen::Quaternion<double> enu_orientation_;

  double vel_ins_x = 0, vel_ins_y = 0, vel_ins_z = 0;
  double ang_ins_x = 0, ang_ins_y = 0, ang_ins_z = 0;

  void ImuTransformerNodelet::onInit(){

    nh_in_ = ros::NodeHandle(getNodeHandle(), "imu_in");
    nh_out_ = ros::NodeHandle(getNodeHandle(), "imu_out");
    private_nh_ = getPrivateNodeHandle();

    private_nh_.param<std::string>("target_frame", target_frame_, "imu_link");
    private_nh_.param<bool>("NED_rpy_reset", ned_rpy_reset_, true);
    private_nh_.param<std::string>("INS_frame_id", ins_frame_id_, "ins_enu");
    private_nh_.param<std::string>("odom_header_frame", odom_header_frame_, "odom");

    tf2_.reset(new tf2_ros::Buffer());
    tf2_listener_.reset(new tf2_ros::TransformListener(*tf2_));

    imu_sub_.subscribe(nh_in_, "data", 10);
    imu_filter_.reset(new ImuFilter(imu_sub_, *tf2_, target_frame_, 10, nh_in_));
    imu_filter_->registerCallback(boost::bind(&ImuTransformerNodelet::imuCallback, this, _1));
    imu_filter_->registerFailureCallback(boost::bind(&ImuTransformerNodelet::failureCb, this, _2));

    mag_sub_.subscribe(nh_in_, "mag", 10);
    // TF Message filter doesn't support ShapeShifter, which we use for Mag messages. Uncomment when IMU Rep goes into
    // effect and we don't need to support two types of Mag message.
//      mag_filter_.reset(new MagFilter(mag_sub_, *tf2_, target_frame_, 10, nh_in_));
//      mag_filter_->registerCallback(boost::bind(&ImuTransformerNodelet::magCallback, this, _1));
//      mag_filter_->registerFailureCallback(boost::bind(&ImuTransformerNodelet::failureCb, this, _2));
    mag_sub_.registerCallback(boost::bind(&ImuTransformerNodelet::magCallback, this, _1));


    
    initial_ned_rpy_ = true;
    ins_odom_pub_ = nh_out_.advertise<nav_msgs::Odometry>("/ins/odometry", 1000);

    ins_odom_sub_ = private_nh_.subscribe("/ins", 1000, ins_ned_Callback);




  }

  void ins_ned_Callback(const nav_msgs::Odometry::ConstPtr& odom_ins){

    vel_ins_x = odom_ins->twist.twist.linear.x;
    vel_ins_y = odom_ins->twist.twist.linear.y;
    vel_ins_z = odom_ins->twist.twist.linear.z;

    ang_ins_x = odom_ins->twist.twist.angular.x;
    ang_ins_y = odom_ins->twist.twist.angular.y;
    ang_ins_z = odom_ins->twist.twist.angular.z;

  }

  void ImuTransformerNodelet::imuCallback(const ImuMsg::ConstPtr &imu_in)
  {
    if(imu_pub_.getTopic().empty()){
      imu_pub_ = nh_out_.advertise<ImuMsg>("data", 10);
    }

    try
    {
      ImuMsg imu_out;

        // std::cout << "IN" << std::endl;

        if(initial_ned_rpy_){
        

          static tf2_ros::StaticTransformBroadcaster static_broadcaster;
          geometry_msgs::TransformStamped static_transformStamped;
          

          

          static_transformStamped.header.stamp = ros::Time::now();
          static_transformStamped.header.frame_id = "odom_ned";
          static_transformStamped.child_frame_id = "odom_enu";
          static_transformStamped.transform.translation.x = 0;
          static_transformStamped.transform.translation.y = 0;
          static_transformStamped.transform.translation.z = 0;
          tf2::Quaternion quat;
          quat.setRPY(3.1416, 0, 1.5708);
          static_transformStamped.transform.rotation.x = quat.x();
          static_transformStamped.transform.rotation.y = quat.y();
          static_transformStamped.transform.rotation.z = quat.z();
          static_transformStamped.transform.rotation.w = quat.w();
          static_broadcaster.sendTransform(static_transformStamped);

          initial_ned_rpy_ = false;

        }

      
        tf2_->transform(*imu_in, imu_out, target_frame_);
        // tf2_->transform(imu_in_new, imu_out, target_frame_);
        imu_pub_.publish(imu_out);

       
        // std::cout << "Waiting for transform" << std::endl;

        try{
          transformStamped_ = tfBuffer.lookupTransform("odom_enu", "gps_link_ned",  
                                    ros::Time(0));
        }
        catch (tf2::TransformException &ex) {
          ROS_WARN("%s",ex.what());
          ros::Duration(1.0).sleep();
        }


        odom_trans_enu_.transform.rotation = imu_out.orientation;
        odom_msg_.pose.pose.orientation = imu_out.orientation;

        // std::cout << "Received transform" << std::endl;

        pos_x_ = transformStamped_.transform.translation.y;
        pos_y_ =  -transformStamped_.transform.translation.x;
        pos_z_ = 0;

        

        odom_trans_enu_.header.stamp = ros::Time::now();
        odom_trans_enu_.header.frame_id = "odom";
        odom_trans_enu_.child_frame_id = ins_frame_id_;
        odom_trans_enu_.transform.translation.x = pos_x_;
        odom_trans_enu_.transform.translation.y = pos_y_;
        odom_trans_enu_.transform.translation.z = pos_z_;

        odom_msg_.header.stamp = ros::Time::now();
        odom_msg_.header.frame_id = "odom";
        odom_msg_.child_frame_id = ins_frame_id_;
        odom_msg_.pose.pose.position.x = pos_x_;
        odom_msg_.pose.pose.position.y = pos_y_;
        odom_msg_.pose.pose.position.z = pos_z_;

        odom_msg_.pose.covariance[0] = 0.01;
        odom_msg_.pose.covariance[4] = 0.01;
        odom_msg_.pose.covariance[8] = 0.01;

        odom_msg_.twist.twist.linear.x = vel_ins_x;
        odom_msg_.twist.twist.linear.y = -vel_ins_y;
        odom_msg_.twist.twist.linear.z = -vel_ins_z;

        odom_msg_.twist.twist.angular.x = ang_ins_x;
        odom_msg_.twist.twist.angular.y = -ang_ins_y;
        odom_msg_.twist.twist.angular.z = -ang_ins_z;

        //send the transform
        odom_broadcaster_enu_.sendTransform(odom_trans_enu_);

        //publish the message
        ins_odom_pub_.publish(odom_msg_);

        return;
      

      
  
    }
    catch (tf2::TransformException ex)
    {
      NODELET_ERROR_STREAM_THROTTLE(1.0, "IMU Transform failure: " << ex.what());
      return;
    }
  }

  // Need to support two types of magnemoter message for now, replace with MagMsg subscriber when IMU REP goes into
  // effect
  void ImuTransformerNodelet::magCallback(const topic_tools::ShapeShifter::ConstPtr &msg)
  {

    std::string error;
    try
    {
      MagMsg::ConstPtr mag_in = msg->instantiate<MagMsg>();

      if(tf2_->canTransform(target_frame_, mag_in->header.frame_id, mag_in->header.stamp, &error)){

        if(mag_pub_.getTopic().empty()){
          mag_pub_ = nh_out_.advertise<MagMsg>("mag", 10);
        }

        MagMsg out;
        tf2_->transform(*mag_in, out, target_frame_);
        mag_pub_.publish(out);
      }else{
        NODELET_WARN_STREAM_THROTTLE(1.0, error);
      }
      return;
    }
    catch (topic_tools::ShapeShifterException &e) {
      NODELET_DEBUG_STREAM(e.what());
    }

    try
    {
      geometry_msgs::Vector3Stamped::ConstPtr mag_in = msg->instantiate<geometry_msgs::Vector3Stamped>();

      if(tf2_->canTransform(target_frame_, mag_in->header.frame_id, mag_in->header.stamp, &error)){

        if(mag_pub_.getTopic().empty()){
          mag_pub_ = nh_out_.advertise<geometry_msgs::Vector3Stamped>("mag", 10);
        }

        MagMsg mag_temp_in, mag_temp_out;
        geometry_msgs::Vector3Stamped mag_out;

        mag_temp_in.header = mag_in->header;
        mag_temp_in.magnetic_field = mag_in->vector;
        tf2_->transform(mag_temp_in, mag_temp_out, target_frame_);
        mag_out.header = mag_temp_out.header;
        mag_out.vector = mag_temp_out.magnetic_field;

        mag_pub_.publish(mag_out);
      }else{
        NODELET_WARN_STREAM_THROTTLE(1.0, error);
      }
      return;
    }
    catch (topic_tools::ShapeShifterException &e) {
      NODELET_DEBUG_STREAM(e.what());
    }

    NODELET_ERROR_STREAM_THROTTLE(1.0, "imu_transformer only accepts sensor_msgs::MagneticField and "
          "geometry_msgs::Vector3Stamped message types on the imu_in/mag_in topic, received " << msg->getDataType());

  }

  void ImuTransformerNodelet::failureCb(tf2_ros::filter_failure_reasons::FilterFailureReason reason)
  {
    NODELET_WARN_STREAM_THROTTLE(1.0, "Can't transform incoming IMU data to " << target_frame_ << " " << 
        reason);
  }

}

PLUGINLIB_EXPORT_CLASS(imu_transformer::ImuTransformerNodelet, nodelet::Nodelet)
