#include <ros/ros.h>
#include "sensor_msgs/Imu.h"
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <nav_msgs/Odometry.h>
#include <string>


std::string header_frame = "odom";
std::string child_frame = "ins_enu";

ros::Publisher ins_odom_pub;
ros::Subscriber imu_enu;

nav_msgs::Odometry odom_msg;
tf::TransformBroadcaster odom_broadcaster;
geometry_msgs::TransformStamped odom_trans;

tf::TransformListener listener;
tf::StampedTransform transform;

ros::Time current_time;

double pos_x = {}, pos_y = {}, pos_z = {};


void imu_callback(const sensor_msgs::Imu::ConstPtr& msg)
{
  current_time = ros::Time::now();

  odom_trans.transform.rotation = msg->orientation;
  odom_msg.pose.pose.orientation = msg->orientation;

  std::cout << "Waiting for transform" << std::endl;
  try{
    listener.lookupTransform("/odom", "/gps_link_ned",  
                              ros::Time(0), transform);
  }
  catch (tf::TransformException ex){
    ROS_ERROR("%s",ex.what());
    ros::Duration(1.0).sleep();
  }

  std::cout << "Received transform" << std::endl;

  pos_x = transform.getOrigin().y();
  pos_y = transform.getOrigin().x();
  pos_z = -transform.getOrigin().z();

  odom_trans.header.stamp = current_time;
  odom_trans.transform.translation.x = pos_x;
  odom_trans.transform.translation.y = pos_y;
  odom_trans.transform.translation.z = pos_z;

  odom_msg.header.stamp = current_time;
  odom_msg.pose.pose.position.x = pos_x;
  odom_msg.pose.pose.position.y = pos_y;
  odom_msg.pose.pose.position.z = pos_z;

  //send the transform
  odom_broadcaster.sendTransform(odom_trans);

  //publish the message
  ins_odom_pub.publish(odom_msg);

}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "ins_ned_enu");

  ros::NodeHandle n;

  odom_trans.header.frame_id = header_frame;
  odom_trans.child_frame_id = child_frame;

  odom_msg.header.frame_id = header_frame;
  odom_msg.child_frame_id = child_frame;
  

  ins_odom_pub = n.advertise<nav_msgs::Odometry>("/ins/odometry", 1000);
  imu_enu = n.subscribe("/imu/data", 1000, imu_callback);
  
  
  ros::spin();


  return 0;
}
