#ifndef IMU_TRANSFORMER_IMU_TRANSFORMER_NODELET
#define IMU_TRANSFORMER_IMU_TRANSFORMER_NODELET

#include "ros/ros.h"
#include "tf2_ros/buffer.h"
#include "tf2_ros/transform_listener.h"
#include "tf2_ros/message_filter.h"
#include "sensor_msgs/Imu.h"
#include "sensor_msgs/MagneticField.h"
#include "nodelet/nodelet.h"
#include "message_filters/subscriber.h"
#include "topic_tools/shape_shifter.h"
#include <tf/LinearMath/Quaternion.h>
#include <tf/tf.h>
#include <Eigen/Core>
#include <string>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <nav_msgs/Odometry.h>

namespace imu_transformer
{
  typedef sensor_msgs::Imu ImuMsg;
  typedef sensor_msgs::MagneticField MagMsg;
  typedef message_filters::Subscriber<ImuMsg> ImuSubscriber;
//  typedef message_filters::Subscriber<MagMsg> MagSubscriber;
  typedef message_filters::Subscriber<topic_tools::ShapeShifter> MagSubscriber;
  typedef tf2_ros::MessageFilter<ImuMsg> ImuFilter;
  typedef tf2_ros::MessageFilter<MagMsg> MagFilter;

  ros::Publisher ins_odom_pub_;
  nav_msgs::Odometry odom_msg_;
  nav_msgs::Odometry odom_msg_ins_;
  ros::Subscriber ins_odom_sub_;

  tf2_ros::TransformBroadcaster odom_broadcaster_enu_;
  geometry_msgs::TransformStamped odom_trans_enu_;
  geometry_msgs::TransformStamped transformStamped_;

  void ins_ned_Callback(const nav_msgs::Odometry::ConstPtr& msg);

  
  double pos_x_ = {}, pos_y_ = {}, pos_z_ = {};

  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);

  std::string ins_frame_id_,target_frame_, odom_header_frame_;

  class ImuTransformerNodelet : public nodelet::Nodelet
  {

  public:
    ImuTransformerNodelet() {};

  private:

    ros::NodeHandle nh_in_, nh_out_, private_nh_;
    boost::shared_ptr<tf2_ros::Buffer> tf2_;
    boost::shared_ptr<tf2_ros::TransformListener> tf2_listener_;

    ImuSubscriber imu_sub_;
    MagSubscriber mag_sub_;

    boost::shared_ptr<ImuFilter> imu_filter_;
    boost::shared_ptr<MagFilter> mag_filter_;

    ros::Publisher imu_pub_, mag_pub_;
    

    virtual void onInit();
    void imuCallback(const ImuMsg::ConstPtr &imu_in);
//    void magCallback(const MagMsg::ConstPtr &mag_in);
    void magCallback(const topic_tools::ShapeShifter::ConstPtr &msg);
    void failureCb(tf2_ros::filter_failure_reasons::FilterFailureReason reason);
    

     // For the resetting of the INS_NED_RPY
    bool ned_rpy_reset_;
    bool initial_ned_rpy_;

    

  };

}  // namespace imu_transformer

#endif  // IMU_TRANSFORMER_IMU_TRANSFORMER_NODELET